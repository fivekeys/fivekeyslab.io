---
layout: post
title: Linux Ricing - Part I&#58; Windows Manager
---

This is a series of posts where I'll show off my Linux Ricing, serving a little bit like a tutorial and more like a documentation process for myself.

If you're the TL;DR kind of person, you can skip all that smalltalk until the `Gimme teh codez` section.

# U w0t mate?
First things first: what is Linux Ricing? Ricing is the process of customizing the appearence for your distribution by tweaking or replacing components so you can achieve you desires of look and feel.

Most people do this for fun. It may require days to finallize your ricing, not because it's hard but because along the way you end up discovering even more possiblities and spend some time looking at neat designs.

I highly recommend that every Linux user eventually try to rice your distribution. Not only it is a good learning process, but you can actually become more productive after setting up a good working environment.

The best place to look for design ideas is [/r/unixporn](https://www.reddit.com/r/unixporn). There you will find tons of beauties and many combinations and custom scripts/configs that you can copy. If you're going down that rabbit hole, I suggest that you create yourself a `dotfile` git repository.

# K thx, KDE much?
The Desktop Environment is the first thing that comes to mind when talking about customizing Linux appearence. It is not the path we're going.

KDE, GNOME, Xfce and so on, each has different levels of personalization potential, performance and nice designs. But we are here to make our Linux unique, and to do so we will have much more freedom by cherry-picking the components we want for each part of the graphical environment.

Today, we are going to talk about Window Managers. This software has only one responsability, and I'll leave as a creativity exercise to find out what it is. Hint: it has to do with windows, and management.

# Windows, bruh
You might think that a Window Manager is not much. What does it have to do, drag stuff around? Well, that too. But in my opinion, the fact that managing windows has gone unnoticed by you so far is evidence that yours is weak.

On Windows, people just love dragging stuff around, clicking on X's to close stuff and clicking on scrollbar arrows. As a Linux user, you probably think that the more keyboard control you have, the faster you get. How you're going to spend all those seconds you save every year, only you can tell me.

Of course, using your mouse you can do about anything. Even Windows has multiple workspaces now, so it's not that behind standard Desktop Environments anymore. Also, keyboard shortcuts can be made anyway, so why a custom WM is
better?

Because tiling. In 99% of your routine, you are not actively making use of floating windows. Sometimes that's quite useful, like putting a small text editor on top of your browser so you can transcribe something not copyable, but in general you want to see the whole application, or you don't want to see it
at all.

That's why tiling WMs are awesome. They tile your windows in a practical way, easily changing between windows and moving them around, dealing with resizing and positioning so you don't waste a single pixel (unless you want to). Here's a sample from that subreddit with neat tiling:

[screen](http://imgur.com/a/75A4y)

Guess what? Those gaps are automatic. I don't use gaps, but they are quite pretty. I'm not an extravagant person, so I'd rather have productivity over beauty. You don't have to follow my taste, though, and could build something like this:

[screen](https://imgur.com/a/thZiW#oUunJp9)

Awesome, right?

# Gimme teh codez
I won't go over all the available options. Instead, I'll show my favorite one and a detailed configuration. This way, when you look for alternatives you will have a good notion of basic functionality you're interested in.

My WM of choice is [i3wm], a minimal and clean tiling WM. Installing it on Debian based distributions is as easy as

`sudo apt-get install i3wm`

Upon installation, i3 will ask you about a few configuration options. That's the bare minimum, and it will generate a .i3/config file that is loaded every time you sign in. All the keybindings are there for you to see and change.

I don't use the standard Vim bindings because I prefer the homerow position, so the very first thing I change is:

Now, because you have multiple WMs (i3 and the one that came with your Desktop Environment), you can choose which to load on Signin.
